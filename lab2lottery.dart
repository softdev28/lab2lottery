import 'package:lab2lottery/lab2lottery.dart' as lab2lottery;

void isPrime(int num) {
  if (num <= 1) {
    print("$num is a prime number");
  } else {
    for (int i = 2; i < num; i++) {
      if ((num % i == 0)) {
        print("$num is a non-prime number");
        return;
      }
    }
    print("$num is a prime number");
  }
}

void Lottery(var ans, var number) {
  if (ans == number) {
    print("you win a lottery");
  } else {
    print("you did not win a lottery");
  }
}

void main() {
  isPrime(3);
  isPrime(2);
  Lottery("123456", "123456");
}
